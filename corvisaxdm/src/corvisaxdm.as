package
{
	import com.adobe.serialization.json.JSON;
	import com.corvisa.one.DynamicConnection;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.StatusEvent;
	import flash.external.*;
	import flash.net.LocalConnection;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.system.Security;
	
	[SWF( height="1", width="1", backgroundColor="#FFFFFF")]
	public class corvisaxdm extends Sprite
	{
		private static var INITALIZED:Boolean = false;
		private var loader:URLLoader;
		private var request:URLRequest;
		private var origin:String;
		private var protocol:String;
		private var domain:String;
		private var port:String;
		private var callback:String;
		private var ns:String;
		private var logging:Boolean;
		private var log:Function;
		
		
		
		protected var sendMap:Object;
		protected var connectionMap:Object = {};
		protected var prefix:String;
		public var data:String;
		
		
		private static function Validate( input:String):Boolean{
			var len:Number = input.length;
			while( len-- ){
				var charCode:Number  = input.charCodeAt(len);
				
				if(  (charCode >= 64 && charCode <= 90 /*Uppercase*/) || 
					 (charCode >= 97 && charCode <= 122 /*Lowercase*/) ){
					continue;
				}
				
				if( charCode >= 48 && charCode <= 57 /*Numbers*/ ){
					continue; 
				}
				
				if( charCode == 95 /*_*/ || 
					charCode == 36 /*$*/ || 
					charCode == 45 /*-*/ || 
					charCode == 46 /*.*/ || 
					charCode == 58 /*:*/ ){
					continue
				}
				return false;
			}
			return true;
		};
		public function corvisaxdm()
		{
			if( INITALIZED ){
				return;
			}
			
			
			connectionMap = {};
			sendMap = {};
			ns   	  = loaderInfo.parameters.ns;
			protocol     = loaderInfo.parameters.proto || "";
			domain    = loaderInfo.parameters.domain || "";
			port      = loaderInfo.parameters.port || "";
			callback  = loaderInfo.parameters.callback || "";
			logging   = loaderInfo.parameters.logging === "true" ? true : false;
			
			origin = protocol + "//" + domain + ":" + port;

		
			log = logging ? function( msg:String ):void{
				
				ExternalInterface.call( loaderInfo.parameters['log'], "swf: " + msg );
			} : trace;
			
			if( !Validate( ns ) || !Validate( protocol ) || !Validate( domain ) || !Validate( port ) || ! Validate( callback ) ){
				log("unable to validate requrested url paremeters " + protocol + " " + domain + " " + port + " "+ callback );
				return;
			}
			log( " Origin: " + origin );
			ExternalInterface.addCallback("reload", this.reload);
			ExternalInterface.addCallback('postMessage', this.postMessage );
			ExternalInterface.addCallback("createChannel", this.createChannel);	
			ExternalInterface.addCallback("destroyChannel", this.destroyChannel);	
			INITALIZED = true;
			Security.allowDomain( "*" );
			Security.allowInsecureDomain( "*" );
	
			ExternalInterface.call( loaderInfo.parameters['onLoad'] );

		}
		
		
		protected function reload( fn:String ): void{
			data = fn;
			var fnName:String = loaderInfo.parameters[data];
			ExternalInterface.call(  fnName );
			
		}
		
		protected function destroyChannel( channel:String ) :void{
			delete sendMap[ channel ];
			connectionMap[ channel ].close();
			delete connectionMap[ channel ];
		}
		
		protected function createChannel( channel:String, secret:String, remoteOrigin:String, isHost:Boolean ): void{
			
			var listeningConnection:DynamicConnection;
			var sendingConnection:DynamicConnection;
			var localSwfDomain:String;
			var incommingFragments:Array = [];
			var maxMessageLength:Number = 40000
			var remoteDomain:String = remoteOrigin.substr(remoteOrigin.indexOf("://") + 3);
			var allowed:String;
			var res:String;
			
			
			if (remoteDomain.indexOf(":") != -1){ 
				remoteDomain = remoteDomain.substr(0, remoteDomain.indexOf(":"));
			}		
			var sendingChannelName:String =  "_" + channel + "_" + secret + "_" +  (isHost ? "consumer" : "provider");
			var receivingChannelName:String = "_" + channel + "_" + secret + "_" + (isHost ? "provider" : "consumer");
			log("create a new channel "  + channel);
			
			
			listeningConnection = connectionMap[channel] = new DynamicConnection();
			localSwfDomain = listeningConnection.domain;
			log( "verify " + domain + " against " + remoteDomain + " "+ localSwfDomain );
			
			//allowed = (domain == remoteDomain || domain == domain || domain == localSwfDomain);
			if( domain == localSwfDomain ){
				allowed = localSwfDomain;
			} else if( domain == remoteDomain ){
				allowed = remoteDomain; 
			} else if( domain == domain ){
				allowed = "*";
			}
			
			log("Domain Allowed " + allowed );
			if( allowed ){
				log('Verified: ' + allowed );
				
				listeningConnection.allowDomain("*" );
				listeningConnection.allowInsecureDomain( "*" );
			} 
			
			
			listeningConnection.onMessage = function( message:String, fromOrigin:String, remaining:Number):void{
				log("listening connection - onMessage " + message + " " +  fromOrigin + " " + remaining);
				
//				if( fromOrigin != remoteOrigin ){
//					log( "received message from " + fromOrigin + ", expected " + remoteOrigin );
//					return;
//				}
				
				incommingFragments.push( message );
				
				if( remaining <= 0 ){
					log( "recieved final fragements");
					ExternalInterface.call( loaderInfo.parameters[ "flash_" + channel + "_onMessage"],  incommingFragments.join("").split("\\").join("\\\\"), remoteOrigin );
					incommingFragments = [];
				} else {
					log( "recieve fragement, length: " + message.length + "remaining: "+ remaining )
				}
			}
				
			listeningConnection.onReady = function( ):void{
				log("listening connection - onReady " );
				if( isHost ){
					log("calling host onReady");
					sendingConnection.send( sendingChannelName, "onReady");
					log("calling " + "flash_"+channel+"_init");
					ExternalInterface.call( loaderInfo.parameters["flash_" + channel + "_init"] );
				}
			}	
		
			// SET UP SENDING CONNECTION
			sendingConnection = new DynamicConnection();
			sendingConnection.addEventListener('status', function( evt:StatusEvent ):void{
				log('sending connection - status event Code: '+ evt.code + ' Level: ' + evt.level);
			}, false, 0, true );
			
			listeningConnection.connect( receivingChannelName );
			sendMap[ channel ] = function( message:String ):void{
				log("pushing message");
				var fragments:Array,
					fragment:String,
					pos:Number,
					length:Number;
				
				// used to "chunk" messages becuse of the 40K limit on LocalConenction
				pos = 0;
				length = message.length;
				
				
				while( pos <= length ){
					fragment = message.substr( pos, maxMessageLength );
					pos += maxMessageLength;
					log("Sending to: " + sendingChannelName )
					sendingConnection.send( sendingChannelName, 'onMessage', fragment, origin, ( length - pos ) ) 

					
				}
			}; // end sendmap fn
			
			if( !isHost ){
				log("I'm not the host")
				
				sendingConnection.send( sendingChannelName, "onReady");
				ExternalInterface.call( loaderInfo.parameters["flash_" + channel + "_init"] );
				
			} else{
				log("im the host");
			}

		}
		
		protected function postMessage( channel:String, message:String ): void{
			log("sending to " + channel + " " + message);
			sendMap[ channel ]( message );
		}
		
	}
} 